//UNA MANERA DE VERIFICAR NAVEGADOR COMPATIBLE
// window.onload = function(){
//     if (!window.openDatabase){
//         alert('Este navegador no soporta el API WebSql');
//     }
// }

//OTRA MANERA DE VERIFICAR NAVEGADOR COMPATIBLE
(function() {
    if (!window.openDatabase) {
        alert('Este navegador no soporta el API WebSql');
    }
})();

let dbComercio;

function openDB() {
    console.log("Abriendo base de datos");
    dbComercio = openDatabase(
        "dbComercio",
        "1.0",
        "Esta es una base de datos del lado del cliente",
        3 * 1024 * 1024
    );

    //VERIFICA SI LA BASE DE DATOS FUE CREADA
    if (!dbComercio) {
        alert("Lamentablemente la base de datos no fue creada");
        javascript_abort();
    } else {
        console.log("Se creó la base dbComercio");
    }

    function javascript_abort() {
        throw new error("Por no abrir la base de datos abortamos javascript");
    }
    //----------------------------------------

    let version = dbComercio.version;
    console.log(version);
}

function creaTabla() {
    console.log("Creando tabla");
    dbComercio.transaction(function (tx) {
        tx.executeSql(
            "CREATE TABLE IF NOT EXISTS Articulos (id integer primary key autoincrement, descripcion, imagen)"
        );
    });
}

let producto = {
    codigo: 0,
    descripcion: "",
    imagen: "",
};

function agregar() {
    creaTabla();
    console.log("Agregando registro");
    dbComercio.transaction(function (tx) {
        // ID NO SE CARGA PORQUE ES AUTOINCREMENT
        producto.descripcion = document.querySelector("#desc").value;
        producto.imagen = document.querySelector("#dirImg").value;
        if (producto.descripcion.trim().length === 0 || producto.descripcion.trim().length === 0) return;
        tx.executeSql(
            "INSERT INTO Articulos (descripcion, imagen) VALUES (?,?)",
            [producto.descripcion, producto.imagen],
            itemInserted
        );
    });
    muestraTabla();
}
function itemInserted(tx, results) {
    console.log("id:", results.insertId);
}

function dropTable() {
    dbComercio.transaction(function (tx) {
        tx.executeSql(
            "DROP TABLE Articulos",
            [],
            function (tx, results) {
                console.log("Table Dropped");
            },
            function (tx, error) {
                console.error("Error: " + error.message);
            }
        );
    });
    document.querySelector("#producto").innerHTML = "";
}
let cantidad;
function muestraTabla() {
    console.log("Mostrando tabla");
    dbComercio.transaction(function (tx) {
        tx.executeSql("SELECT * FROM Articulos", [], function (tx, results) {
            cantidad = results.rows.length;
            armaTemplate(results);
        });
    });
}

function armaTemplate(results) {
    let template = '', row;
    console.log(results);
    for (let i = 0; i < cantidad; i++) {
        row = results.rows.item(i);

        producto.codigo = row.codigo;
        producto.descripcion = row.descripcion;
        producto.imagen = row.imagen;

        template += `<article>
                        <h3 class="descripcion">${producto.descripcion}</h3>
                        <img src="${producto.imagen}" class="imagen">
                    </article>`
    }
    document.querySelector("#producto").innerHTML = template;
}

function listado() {
    let arrayProductos = [], row;
    dbComercio.transaction(function (tx) {
        tx.executeSql("SELECT * FROM Articulos", [], function (tx, results) {
            cantidad = results.rows.length;
            for (let i = 0; i < cantidad; i++) {
                row = results.rows.item(i);
                console.log(row);
                arrayProductos[i] = row;
            }
            if (cantidad > 0) {
                localStorage.setItem('articulos', JSON.stringify(arrayProductos));
                location.href = 'resultados.html';
            }
        });
    });


}

function main() {
    openDB();
    creaTabla();
    muestraTabla();
}

main();

